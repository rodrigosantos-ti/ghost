//
//  ChatViewController.swift
//  Ghost
//
//  Created by Rodrigo Santos on 03/12/2018.
//  Copyright © 2018 Rodrigo Santos. All rights reserved.
//

import UIKit
import SocketIO

class ChatViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeClient()
    }
    
    
    func initializeClient() {
        let manager = SocketManager(socketURL: URL(string: "http://192.168.1.122:4666")!, config: [.log(true), .compress])
        let socket = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            print("Socket connected")
        }
        socket.on("news") {data, ack in
            print("Received 'news' event")
            print(data)
        }
//        socket.on("health") {data, ack in
//            print("Received 'health' event")
//            print(data)
//        }
        socket.connect()
        CFRunLoopRun()
    }

}
